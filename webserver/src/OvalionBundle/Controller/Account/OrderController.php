<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 29/10/2018
 * Time: 18:18
 */

namespace OvalionBundle\Controller\Account;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\Account\Order;
use OvalionBundle\Entity\Account\User;
use OvalionBundle\Entity\Trips\Travel;
use OvalionBundle\Repository\Account\OrderRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class OrderController
 * @package OvalionBundle\Controller\Account
 * @Route("/orders")
 */
class OrderController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Account")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->getDoctrine()->getRepository(Order::class);
        try {
            parent::getAction($request);
            $id = $request->get("id");
            $allOrders = null;
            /** @var User $user */
            $user = $this->getApiUser();
            if (is_null($id)) {
                $allOrders = $orderRepository->apiFindByUser($user);
            } else {
                $allOrders = $orderRepository->apiFind($id);
            }
            return new JsonResponse($allOrders, 200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }

    /**
     * @Route("", methods={"POST"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *     @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="idBus", type="integer"),
     *                 @SWG\Property(property="idHotel", type="integer"),
     *                 @SWG\Property(property="idMatch", type="integer"),
     *                 @SWG\Property(property="ticketType", type="integer"),
     *                 @SWG\Property(property="date_start", type="string"),
     *                 @SWG\Property(property="date_end", type="string"),
     *                 @SWG\Property(property="payment_ref", type="string"),
     *                 @SWG\Property(property="price", type="integer"),
     *            )
     *     )
     * )
     *
     * @SWG\Response(
     *         response="202",
     *         description="Contenue créer"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Account")
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        try {
            parent::postAction($request);
            $orderRepository = $this->getDoctrine()->getRepository(Order::class);
            $entityManager = $this->getDoctrine()->getManager();
            $content = json_decode($request->getContent(), true);
            $travel = $this->get('ovalion.helper.travel_by_order')->create($content);
            if (!$travel instanceof Travel)
            {
                return new JsonResponse($travel, 500);
            }
            $newOrder = new Order();
            $newOrder->setPrice($content[0]['price']);
            $id = count($orderRepository->findAll()) + 1;
            $newOrder->setId($id);
            $newOrder->setTravel($travel);
            $newOrder->setPaidAt(new \DateTime());
            $newOrder->setPaymentRef($content[0]["payment_ref"]);
            $newOrder->setUser($this->getApiUser());
            $entityManager->persist($newOrder);
            $this->getApiUser()->addOrder($newOrder);
            $entityManager->flush();
            return new JsonResponse("Content created", 201);

        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }
    /**
     * @Route("/cancel/{id}", methods={"GET"})
     * @param Request $request
     * @SWG\Response(
     *         response="200",
     *         description="Contenu supprimé"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Account")
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        try {
            parent::deleteAction($request);
            $orderRepository = $this->getDoctrine()->getRepository(Order::class);
            $entityManager = $this->getDoctrine()->getManager();
            /** @var Order $order */
            $order = $orderRepository->findOneBy(['id' => $request->get('id')]);
            $order->setStatus(Order::CANCELLED);
            $entityManager->flush();
            return new JsonResponse("Order Canceled", 200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

    }
}