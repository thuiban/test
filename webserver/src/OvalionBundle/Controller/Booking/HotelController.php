<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 23/10/2018
 * Time: 19:26
 */

namespace OvalionBundle\Controller\Booking;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\Booking\Hotel;
use OvalionBundle\Repository\Booking\HotelRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class HotelController
 * @package OvalionBundle\Controller\Booking
 * @Route("/hotel")
 */
class HotelController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Booking")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $this->getDoctrine()->getRepository(Hotel::class);
        try {
            parent::getAction($request);
            $id = $request->get("id");
            $allHotels = null;
            if (is_null($id)) {
                $allHotels = $hotelRepository->apiFindAll();
            } else {
                $allHotels = $hotelRepository->apiFind($id);
            }
            return new JsonResponse($allHotels, 200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), "500");
        }
    }

}