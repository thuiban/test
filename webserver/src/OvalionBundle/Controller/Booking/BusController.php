<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 23/10/2018
 * Time: 18:30
 */

namespace OvalionBundle\Controller\Booking;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\Booking\Bus;
use OvalionBundle\Repository\Booking\BusRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Swagger\Annotations as SWG;

/**
 * Class BusController
 * @package OvalionBundle\Controller\Booking
 * @Route ("/bus")
 */
class BusController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Booking")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        /** @var BusRepository $busRepository */
        $busRepository = $this->getDoctrine()->getRepository(Bus::class);
        try {
            parent::getAction($request);
            $id = $request->get("id");
            $allBuses = null;
            if (is_null($id)) {
                $allBuses = $busRepository->apiFindAll();
            } else {
                $allBuses = $busRepository->apiFind($id);
            }
           return new JsonResponse($allBuses, 200);
        } catch (\Exception $e) {
           return new JsonResponse($e->getMessage(), "500");
        }
    }
}