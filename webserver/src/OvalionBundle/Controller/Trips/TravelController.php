<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 24/10/2018
 * Time: 12:59
 */

namespace OvalionBundle\Controller\Trips;


use Doctrine\Common\Collections\ArrayCollection;
use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\Account\Order;
use OvalionBundle\Entity\Trips\Travel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class TravelController
 * @package OvalionBundle\Controller\Trips
 * @Route("/trips")
 */
class TravelController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Trips")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        parent::getAction($request);
        $user = $this->getApiUser();
        $orderRepository = $this->getDoctrine()->getRepository(Order::class);
        $id = $request->get("id");
        $results = [];
        try {
            /** @var ArrayCollection $allOrders */
            $allOrders = $orderRepository->findBy(['user' => $user]);
            /** @var Order $order*/
            foreach ($allOrders as $order) {
                if (is_null($id)) {
                  $results[] = $order->getTravel();
                } else {
                    if ($order->getTravel()->getId() == $id) {
                        $results[] = $order->getTravel();
                    }
                }
            }
            return new JsonResponse($results, 200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }
}