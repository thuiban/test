<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 16/10/2018
 * Time: 13:23
 */

namespace OvalionBundle\Controller\Authentication;


use OvalionBundle\Entity\Account\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class AuthenticationController extends Controller
{
    /**
     * @Route("/logintoken", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="idToken",
     *          in="query",
     *          required=true,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème de vérification au près de google"
     *     )
     * @SWG\Response(
     *     response="208",
     *     description="Utilisateur déjà connecté"
     * )
     * @SWG\Tag(name="Authentication")
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $idToken = $request->get("idToken");
        $idClient = $this->getParameter("google_id_client");
        $client = new \Google_Client(['client_id' => $idClient]);
        $payload = $client->verifyIdToken($idToken);
        $GoogleUser = $this->get('ovalion.helper.user_by_google');
        if (!$payload) {
            return new JsonResponse("Google verification error", 500);
        }
        /** @var User $user */
        if ($this->get('session')->get("name") == $payload["name"]) {
            return new JsonResponse("Already connected", 208);
        }

        $user = $GoogleUser->getIt($payload);
        $session = new Session();
        $session->set("email", $user->getEmail());
        $session->set("name", $user->getUsername());
        $session->set("id", $user->getId());
        return new JsonResponse($user->getUsername()." has been connected", 200);
    }

    /**
     * @Route("/logout", methods={"GET"})
     * @param Request $resquest
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Server"
     *     )
     * )
     * @SWG\Tag(name="Authentication")
     * @return JsonResponse
     */
    public function logoutAction(Request $request)
    {
        $this->get('session')->invalidate();
        return new JsonResponse("User disconnected", 200);
    }
}