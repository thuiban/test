<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 23/10/2018
 * Time: 18:17
 */

namespace OvalionBundle\Controller;


use OvalionBundle\Entity\Account\User;
use OvalionBundle\Repository\Account\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseController extends Controller
{
    public function getAction(Request $request)
    {
        $this->checkAcl();
    }

    public function postAction(Request $request)
    {
        $this->checkAcl();
    }

    public function putAction(Request $request)
    {
        $this->checkAcl();
    }

    public function deleteAction(Request $request)
    {
        $this->checkAcl();
    }

    public function checkAcl()
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            $user = $this->get("session")->get("name");
            if (!$user) {
                return new JsonResponse("Not connected", 401);
            }
        }
    }

    /**
     * @return null|User
     */
    public function getApiUser()
    {
        $email = $this->get("session")->get("email");
        /** @var UserRepository $userRepository **/
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        /** @var User $res */
        $res = $userRepository->findOneBy(['email' => $email]);
        return $res;

    }
}