<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 05/11/2018
 * Time: 14:44
 */

namespace OvalionBundle\Controller\Parameters;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\MyTeam\Team;
use OvalionBundle\Entity\Parameters\Settings;
use OvalionBundle\Repository\Parameters\SettingsRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class SettingsController
 * @package OvalionBundle\Controller\Parameter
 * @Route("/settings")
 */

class SettingsController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Parameters")
     * @return JsonResponse
     */
    public function getAction (Request $request){
        /** @var SettingsRepository $settingsRepository */
        $settingsRepository = $this->getDoctrine()->getRepository(Settings::class);
        try {
            parent::getAction($request);
            $settings = $settingsRepository->apiFind($this->getApiUser()->getSettings()->getId());
            return new JsonResponse($settings, 200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }

    /**
     * @Route("", methods={"PUT"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *     @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="idTeam", type="integer"),
     *                 @SWG\Property(property="lang", type="string")
     *            )
     *     )
     * )
     * @SWG\Response(
     *         response="200",
     *         description="Contenue modifié"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Parameters")
     * @return JsonResponse
     */

    public function putAction(Request $request)
    {
        try {
            parent::putAction($request);
            $entityManager = $this->getDoctrine()->getManager();
            $settingsRepository = $this->getDoctrine()->getRepository(Settings::class);
            /** @var Settings $currentSetting */
            if ( $settingsRepository->find($this->getApiUser()->getSettings()->getId()) == null) {
                $currentSetting = new Settings();
            } else {
                $currentSetting =$settingsRepository->find($this->getApiUser()->getSettings()->getId());
            }
            $teamRepository = $this->getDoctrine()->getRepository(Team::class);
            $content = json_decode($request->getContent(), true)[0];
            $currentSetting->setFavTeam($teamRepository->find($content['idTeam']));
            $currentSetting->setLang($content['lang']);
            if ( $settingsRepository->find($this->getApiUser()->getSettings()->getId()) == null) {
                $entityManager->persist($currentSetting);
            }
            $this->getApiUser()->setSettings($currentSetting);
            $entityManager->flush();
            return new JsonResponse($settingsRepository->apiFind($this->getApiUser()->getSettings()->getId()),200);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }
}