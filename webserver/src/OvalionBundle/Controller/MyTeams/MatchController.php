<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 29/10/2018
 * Time: 13:15
 */

namespace OvalionBundle\Controller\MyTeams;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\Account\User;
use OvalionBundle\Entity\MyTeam\Match;
use OvalionBundle\Entity\MyTeam\Team;
use OvalionBundle\Repository\MyTeam\MatchRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class MatchController
 * @package OvalionBundle\Controller\MyTeams
 * @Route("/match")
 */
class MatchController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Match")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        /** @var MatchRepository $matchRepository */
        $matchRepository = $this->getDoctrine()->getRepository(Match::class);
        try {
            parent::getAction($request);
            /** @var User $user */
            $user = $this->getApiUser();
            /** @var Team $favTeam */
            $favTeam = $user->getSettings()->getFavTeam();
            $id = $request->get("id");
            $allMatchs = null;
            if (is_null($id)) {
                if (is_null($favTeam)) {
                    $allMatchs = $matchRepository->apiFindAll();
                } else {
                    $allMatchs = $matchRepository->apiFindAllFavTeams($favTeam);
                }
            } else {
                $allMatchs = $matchRepository->apiFind($id);
            }
            return new JsonResponse($allMatchs, "200");
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), "500");
        }
    }
}