<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 29/10/2018
 * Time: 13:42
 */

namespace OvalionBundle\Controller\MyTeams;


use OvalionBundle\Controller\BaseController;
use OvalionBundle\Entity\MyTeam\Team;
use OvalionBundle\Repository\MyTeam\TeamRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class TeamController
 * @package OvalionBundle\Controller\MyTeams
 * @Route("/teams")
 */
class TeamController extends BaseController
{
    /**
     * @Route("", methods={"GET"})
     * @param Request $request
     * @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          required=false,
     *          type="string",
     *     )
     * @SWG\Response(
     *         response="200",
     *         description="Réponse valide"
     *     )
     * @SWG\Response(
     *         response="500",
     *         description="Problème serveur"
     *     )
     * @SWG\Response(
     *     response="401",
     *     description="Pas Identifié"
     * )
     * @SWG\Tag(name="Match")
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        /** @var TeamRepository $teamRepository */
        $matchRepository = $this->getDoctrine()->getRepository(Team::class);
        try {
            parent::getAction($request);
            $id = $request->get("id");
            $allTeams = null;
            if (is_null($id)) {
                $allTeams = $matchRepository->apiFindAll();
            } else {
                $allTeams = $matchRepository->apiFind($id);
            }
            return new JsonResponse($allTeams, "200");
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), "500");
        }
    }
}