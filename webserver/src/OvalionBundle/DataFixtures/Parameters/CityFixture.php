<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 05/11/2018
 * Time: 15:33
 */

namespace OvalionBundle\DataFixtures\Parameters;


use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\Parameters\City;

class CityFixture extends BaseFixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->citys as $city)
        {
            $cityObject = new City();
            $cityObject->setName($city);
            $manager->persist($cityObject);
        }
        $manager->flush();
    }
}