<?php

namespace OvalionBundle\DataFixtures\Booking;

use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\Booking\Hotel;
use OvalionBundle\Entity\Parameters\City;

/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 09/10/2018
 * Time: 13:40
 */

class HotelFixture extends BaseFixture
{
    public function load(ObjectManager $manager)
    {
        $cityManager =  $manager->getRepository(City::class);
        foreach ($this->citys as $city) {
            foreach ($this->hotels as $hotelName) {
                $hotel = new Hotel();
                $hotel->setName($hotelName.' '.$city);
                $hotel->setCity($cityManager->findOneBy(['name' => $city]));
                $hotel->setAddress("75 Rue d'Annabelle Paul ".$city);
                $hotel->setPrice(55);
                $manager->persist($hotel);
            }
        }
        $manager->flush();
    }
}