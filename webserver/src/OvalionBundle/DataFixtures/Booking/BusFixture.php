<?php

namespace OvalionBundle\DataFixtures\Booking;

use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\Entity\Booking\Bus;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\Parameters\City;

/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 09/10/2018
 * Time: 13:31
 */

class BusFixture extends BaseFixture
{
    public function load(ObjectManager $manager)
    {
        $cityManager = $manager->getRepository(City::class);
        foreach ($this->citys as $city) {
            foreach ($this->bus as $busCompany) {
                $bus = new Bus();
                $bus->setCapacity(60);
                $bus->setName($city.' '.$busCompany);
                $bus->setCity($cityManager->findOneBy(['name' => $city]));
                $bus->setComplete(false);
                $bus->setPrice(20);
                $bus->setNumber(324);
                $manager->persist($bus);
            }
        }
        $manager->flush();
    }
}