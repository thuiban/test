<?php

namespace OvalionBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 09/10/2018
 * Time: 13:14
 */

class BaseFixture extends Fixture
{

    protected $citys = [
        'Clermont',
        'Paris',
        'Lyon',
        'Montpelier',
        'Castres',
        'Toulouse',
        'Versailles',
        'Bordeaux',
        'La Rochelle',
        'Grenoble',
        'Agen',
        'Perpignan',
        'Pau',
        'Toulon'
    ];

    protected $hotels = [
        'Ibis Budget',
        'Ibis',
        'Kyriad',
        'Campanile',
        'Novotel',
        'Hotel Sympa'
    ];

    protected $bus = [
        'Ouibus - Les BUS SNCF',
        'Flexibus - Flexible sauf le prix',
        'Compagnie Privé',
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {}
}