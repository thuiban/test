<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 09/10/2018
 * Time: 18:11
 */

namespace OvalionBundle\DataFixtures\MyTeam;

use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\MyTeam\Match;
use OvalionBundle\Entity\MyTeam\Team;
use OvalionBundle\Entity\Parameters\City;
use phpDocumentor\Reflection\Types\Integer;

class MatchFixture extends BaseFixture
{
    public function load(ObjectManager $manager)
    {
        $TeamManager = $manager->getRepository(Team::class);
        $CityManager = $manager->getRepository(City::class);
        for ($i = 0; $i < 14; $i = $i + 2)
        {

            $city = $CityManager->findOneBy(['name' => $this->citys[$i]]);
            $city2 = $CityManager->findOneBy(['name' => $this->citys[$i+1]]);
            /** @var Team $team1 */
            $team1 = $TeamManager->findOneBy(['city' => $city]);
            /** @var Team $team2 */
            $team2 = $TeamManager->findOneBy(['city' => $city2]);
            $match = new Match();
            $match->setCity($city);
            $match->setTeams($team1, $team2);
            $match->setDate(new \DateTime());
            $match->setPlace(542);
            $match->setScore("32 - 23");
            $manager->persist($match);
        }
        $manager->flush();
    }
}