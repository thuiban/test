<?php

namespace OvalionBundle\DataFixtures\MyTeam;

use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\MyTeam\Team;
use OvalionBundle\Entity\Parameters\City;

class TeamFixture extends BaseFixture {
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $i = 0;
        $cityManager = $manager->getRepository(City::class);
         foreach ($this->citys as $city) {
             $team = new Team();
             $team->setName("RC " .$city);
             $team->setCity($cityManager->findOneBy(['name' => $city]));
             $team->setRanking($i++);
             $manager->persist($team);
         }
         $manager->flush();
    }
}