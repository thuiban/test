<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 11/10/2018
 * Time: 21:55
 */

namespace OvalionBundle\DataFixtures\Account;


use Doctrine\Common\Persistence\ObjectManager;
use OvalionBundle\DataFixtures\BaseFixture;
use OvalionBundle\Entity\Account\User;

class UserFixture extends BaseFixture
{
    public function load(ObjectManager $manager)
    {
      $user = new User();
      $user->setUsername("administratueur");
      $user->setEmail("theo.huiban@epitech.eu");
      $user->setPlainPassword("tfhR5lxu");
      $user->setRoles(['ROLE_SUPER_ADMIN']);
      $user->setEnabled(true);
      $manager->persist($user);
      $manager->flush();
    }
}