<?php

namespace OvalionBundle\Repository\Booking;

/**
 * HotelController
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HotelRepository extends \Doctrine\ORM\EntityRepository
{
    public function apiFindAll()
    {
        return $this->createQueryBuilder('h')
            ->getQuery()
            ->getArrayResult();
    }

    public function apiFind($id)
    {
        return $this->createQueryBuilder('h')
            ->where("h.id = :id")
            ->setParameter("id", $id)
            ->getQuery()
            ->getArrayResult();
    }
}
