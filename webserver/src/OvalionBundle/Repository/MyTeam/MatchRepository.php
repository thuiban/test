<?php

namespace OvalionBundle\Repository\MyTeam;

use DateTime;

/**
 * MatchRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MatchRepository extends \Doctrine\ORM\EntityRepository
{
    public function apiFindAll()
    {
        $array = $this->createQueryBuilder('m')
            ->getQuery()
            ->getArrayResult();
        $i = 0;
        foreach ($array as $data) {
            /** @var DateTime $date */
            $t1 = $this->findOneBy(['id' => $data['id']])->getTeam1();
            $t2 = $this->findOneBy(['id' => $data['id']])->getTeam2();
            $date = $data['date'];
            $array[$i]['date'] = $date->format("d/m/Y");
            $array[$i]['teamOneId'] = $t1->getId();
            $array[$i++]['teamTwoId'] = $t2->getId();
        }

        return $array;
    }

    public function apiFind($id)
    {
        return $this->createQueryBuilder('m')
            ->where("m.id = :id")
            ->setParameter("id", $id)
            ->getQuery()
            ->getArrayResult();
    }

    public function apiFindAllFavTeams($favTeam)
    {
        $array = $this->createQueryBuilder("m")
            ->where("m.team1 = :team OR m.team2 = :team")
            ->setParameter("team", $favTeam)
            ->getQuery()
            ->getArrayResult();

        return $array;
    }
}
