<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 05/11/2018
 * Time: 15:30
 */

namespace OvalionBundle\Entity\Parameters;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="parameters_city")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\Parameters\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\MyTeam\Match", mappedBy="city")
     */
    private $matchs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\MyTeam\Team", mappedBy="city")
     */
    private $teams;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\Booking\Bus", mappedBy="city")
     */
    private $buses;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\Booking\Hotel", mappedBy="city")
     */
    private $hotels;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\Parameters\Settings", mappedBy="city")
     */
    private $settings;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getMatchs()
    {
        return $this->matchs;
    }

    /**
     * @param ArrayCollection $matchs
     */
    public function setMatchs(ArrayCollection $matchs)
    {
        $this->matchs = $matchs;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param ArrayCollection $teams
     */
    public function setTeams(ArrayCollection $teams)
    {
        $this->teams = $teams;
    }

    /**
     * @param ArrayCollection $buses
     * @return City
     */
    public function setBuses(ArrayCollection $buses)
    {
        $this->buses = $buses;
        return $this;
    }

    /**
     * @param ArrayCollection $hotels
     * @return City
     */
    public function setHotels(ArrayCollection $hotels)
    {
        $this->hotels = $hotels;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param ArrayCollection $settings
     */
    public function setSettings(ArrayCollection $settings)
    {
        $this->settings = $settings;
    }

    public function __toString()
    {
       return $this->getName();
    }

}