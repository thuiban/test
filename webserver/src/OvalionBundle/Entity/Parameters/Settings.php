<?php

namespace OvalionBundle\Entity\Parameters;

use Doctrine\ORM\Mapping as ORM;
use OvalionBundle\Entity\MyTeam\Team;

/**
 * Settings
 *
 * @ORM\Table(name="parameters_settings")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\Parameters\SettingsRepository")
 */
class Settings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Team
     *
     * @ORM\OneToOne(targetEntity=Team::class)
     */
    private $favTeam;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string")
     */
    private $lang = 'fr';

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Parameters\City", inversedBy="settings")
     */
    private $city;

    /**
     * @return Team
     */
    public function getFavTeam()
    {
        return $this->favTeam;
    }

    /**
     * @param Team $favTeam
     */
    public function setFavTeam($favTeam)
    {
        $this->favTeam = $favTeam;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang)
    {
        $this->$lang = $lang;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
    }

    public function __toString()
    {
        return $this->favTeam->getName();
    }
}
