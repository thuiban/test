<?php

namespace OvalionBundle\Entity\MyTeam;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use OvalionBundle\Entity\Parameters\City;
use OvalionBundle\Entity\Trips\Travel;

/**
 * Match
 *
 * @ORM\Table(name="my_team_match")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\MyTeam\MatchRepository")
 */
class Match
{
    const NOT_PLAY = 'NOT_PLAY';
    const PLAYED = 'PLAYED';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\Trips\Travel", mappedBy="match")
     */
    private $travels;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Parameters\City", inversedBy="matchs")
     */
    private $city;

    /**
     * @var int
     *
     * @ORM\Column(name="Score", type="string", length=255)
     */
    private $score;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Place", type="integer", nullable=true)
     */
    private $place;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;

    /**
     * @var Team
     *
     * @ORM\OneToOne(targetEntity="Team")
     */
    private $team1 = null;

    /**
     * @var Team
     *
     * @ORM\OneToOne(targetEntity="Team")
     */
    private $team2 = null;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status = self::NOT_PLAY;

    /**
     * @return Team
     */
    public function getTeam2()
    {
        return $this->team2;
    }

    /**
     * @param Team $Team2
     */
    public function setTeam2($Team2)
    {
        $this->team2 = $Team2;
    }

    public function getName()
    {
        return $this->getTeam1()." - ".$this->getTeam2().": ".$this->getDate()->format("d/m/Y");
    }

    /**
     * @return Team
     */
    public function getTeam1()
    {
        return $this->team1;
    }

    /**
     * @param Team $Team1
     */
    public function setTeam1($Team1)
    {
        $this->team1 = $Team1;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score.
     *
     * @param string $score
     *
     * @return Match
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score.
     *
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set place.
     *
     * @param int|null $place
     *
     * @return Match
     */
    public function setPlace($place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place.
     *
     * @return int|null
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Match
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set Teams with two parameters
     *
     * @param Team $t1
     * @param Team $t2
     *
     */
    public function setTeams(Team $t1, Team $t2)
    {
        $this->setTeam1($t1);
        $this->setTeam2($t2);
    }

    /**
     * Set Team for match
     * @param Team $t
     */
    public function setTeam(Team $t)
    {
        if (is_null($this->getTeam1())) {
            $this->setTeam1($t);
        } elseif (is_null($this->getTeam2())) {
            $this->setTeam2($t);
        }
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }


    /**
     * @param Travel $travel
     */
    public function addTravel($travel)
    {
        $this->travels->add($travel);
    }


    /**
     * @return ArrayCollection
     */
    public function getTravels()
    {
        return $this->travels;
    }

    /**
     * @param ArrayCollection $travels
     */
    public function setTravels(ArrayCollection $travels)
    {
        $this->travels = $travels;
    }

    public function __construct()
    {
        $this->travels = new ArrayCollection();
    }
}
