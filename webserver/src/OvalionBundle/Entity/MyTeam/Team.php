<?php

namespace OvalionBundle\Entity\MyTeam;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use OvalionBundle\Entity\Parameters\City;

/**
 * Team
 *
 * @ORM\Table(name="my_team_team")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\MyTeam\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="ranking", type="string", length=255, nullable=true)
     */
    private $ranking;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Parameters\City", inversedBy="teams")
     */
    private $city;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city.
     *
     * @param City $city
     *
     * @return Team
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * @param int $ranking
     */
    public function setRanking(int $ranking)
    {
        $this->ranking = $ranking;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
