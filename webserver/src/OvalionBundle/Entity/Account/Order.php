<?php

namespace OvalionBundle\Entity\Account;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="account_order")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\Account\OrderRepository")
 */
class Order
{
    const CONFIRM = "CONFIRM";
    const CANCELLED = "CANCELLED";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PaymentRef", type="string", length=255)
     */
    private $paymentRef;

    /**
     * @var int
     *
     * @ORM\Column(name="Price", type="integer")
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PaidAt", type="datetime")
     */
    private $paidAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Travel
     *
     * @ORM\OneToOne(targetEntity=OvalionBundle\Entity\Trips\Travel::class)
     */
    private $travel;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, options={"default":"CONFIRM"})
     */

    private $status = self::CONFIRM;

    /**
     * @return Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * @param Travel $travel
     */
    public function setTravel($travel)
    {
        $this->travel = $travel;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentRef.
     *
     * @param string $paymentRef
     *
     * @return Order
     */
    public function setPaymentRef($paymentRef)
    {
        $this->paymentRef = $paymentRef;

        return $this;
    }

    /**
     * Get paymentRef.
     *
     * @return string
     */
    public function getPaymentRef()
    {
        return $this->paymentRef;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set paidAt.
     *
     * @param \DateTime $paidAt
     *
     * @return Order
     */
    public function setPaidAt($paidAt)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * Get paidAt.
     *
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * set User.
     *
     * @param User $user
     *
     * @return Order
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Set id.
     *
     * @param $id
     *
     * @return Order
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * get User.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

}
