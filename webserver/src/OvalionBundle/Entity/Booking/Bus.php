<?php

namespace OvalionBundle\Entity\Booking;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use OvalionBundle\Entity\Parameters\City;
use OvalionBundle\Entity\Trips\Travel;

/**
 * Bus
 *
 * @ORM\Table(name="booking_bus")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\Booking\BusRepository")
 */
class Bus
{
    const BUS_ARRIVED = "BUS_ARRIVED";
    const BUS_LATE = "BUS_LATE";
    const BUS_ONTIME = "BUS_ONETIME";
    const BUS_RUNNING = "BUS_RUNNING";

    const NO_COMMENT = "NO_COMMENT";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status = self::BUS_ONTIME;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OvalionBundle\Entity\Trips\Travel", mappedBy="bus")
     */
    private $travels;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Parameters\City", inversedBy="buses")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = self::NO_COMMENT;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer")
     */
    private $capacity;

    /**
     * @var bool
     *
     * @ORM\Column(name="complete", type="boolean")
     */
    private $complete;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Bus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return Bus
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set capacity.
     *
     * @param int $capacity
     *
     * @return Bus
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity.
     *
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set complete.
     *
     * @param bool $complete
     *
     * @return Bus
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;

        return $this;
    }

    /**
     * Get complete.
     *
     * @return bool
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


    /**
     * @param Travel $travel
     */
    public function addTravel($travel)
    {
        $this->travels->add($travel);
    }


    /**
     * @return ArrayCollection
     */
    public function getTravels()
    {
        return $this->travels;
    }

    /**
     * @param ArrayCollection $travels
     */
    public function setTravels(ArrayCollection $travels)
    {
        $this->travels = $travels;
    }

    public function __construct()
    {
        $this->travels = new ArrayCollection();
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity(City $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number)
    {
        $this->number = $number;
    }
}
