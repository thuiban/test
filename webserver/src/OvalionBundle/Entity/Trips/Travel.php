<?php

namespace OvalionBundle\Entity\Trips;

use Doctrine\ORM\Mapping as ORM;
use OvalionBundle\Entity\Booking\Bus;
use OvalionBundle\Entity\Booking\Hotel;
use OvalionBundle\Entity\MyTeam\Match;

/**
 * Travel
 *
 * @ORM\Table(name="trips_travel")
 * @ORM\Entity(repositoryClass="OvalionBundle\Repository\Trips\TravelRepository")
 */
class Travel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateStart", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateEnd", type="datetime")
     */
    private $dateEnd;

    /**
     * @var Bus
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Booking\Bus", inversedBy="travels")
     * @ORM\JoinColumn(nullable=true)
     */
    private $bus;

    /**
     * @var Hotel
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\Booking\Hotel", inversedBy="travels")
     * @ORM\JoinColumn(nullable=true)
     */
    private $hotel;


    /**
     * @var Match
     *
     * @ORM\ManyToOne(targetEntity="OvalionBundle\Entity\MyTeam\Match", inversedBy="travels")
     */
    private $match;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $type;

    /**
     * Set id.
     *
     * @param $id
     *
     * @return Travel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param Match $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
    }

    /**
     * @return Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param Hotel $hotel
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
    }

    /**
     * @return Bus
     */
    public function getBus()
    {
        return $this->bus;
    }

    /**
     * @param Bus $bus
     */
    public function setBus($bus)
    {
        $this->bus = $bus;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart.
     *
     * @param \DateTime $dateStart
     *
     * @return Travel
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart.
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd.
     *
     * @param \DateTime $dateEnd
     *
     * @return Travel
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd.
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function __toString()
    {
        return $this->dateStart->format("d/m/Y")." - ".$this->dateEnd->format('d/m/Y');
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }
}
