<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 04/11/2018
 * Time: 16:22
 */

namespace OvalionBundle\Admin\MyTeam;


use OvalionBundle\Entity\MyTeam\Match;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MatchAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $list)
    {
        $list->add("name")
            ->add("status")
            ->add("score")
            ->add("city")
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
      $show->add("name")
          ->add("status")
          ->add("score")
          ->add("city")
          ->add("date")
          ->add("team1")
          ->add("team2");
    }

    protected function configureFormFields(FormMapper $form)
    {
      $form->add("status", ChoiceType::class, ['choices' =>
            [
                "Played" => Match::PLAYED,
                "Not Played" => Match::NOT_PLAY
            ]])
          ->add("score")
          ->add("city")
          ->add("date")
          ->add("team1")
          ->add("team2");
    }
}