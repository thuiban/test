<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 04/11/2018
 * Time: 16:47
 */

namespace OvalionBundle\Admin\MyTeam;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TeamAdmin extends AbstractAdmin
{
    public function configureShowFields(ShowMapper $show)
    {
        $show->add('name')
            ->add('ranking')
            ->add('city');
    }

    public function configureListFields(ListMapper $list)
    {
        $list->add('name')
            ->add('ranking')
            ->add('city')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }

    public function configureFormFields(FormMapper $form)
    {
        $form->add('name')
        ->add('ranking')
        ->add('city');
    }
}