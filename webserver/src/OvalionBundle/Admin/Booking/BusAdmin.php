<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 04/11/2018
 * Time: 15:58
 */

namespace OvalionBundle\Admin\Booking;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BusAdmin extends AbstractAdmin
{
    protected function configureShowFields(ShowMapper $show)
    {
        $show->add("name")
            ->add("status")
            ->add("comment")
            ->add("price")
            ->add("complete")
            ->add("capacity");
    }

    protected function configureListFields(ListMapper $list)
    {
      $list->add("name")
          ->add("status")
          ->add("comment")
          ->add("complete")
          ->add('_action', null, [
              'actions' => [
                  'show' => [],
                  'edit' => [],
                  'delete' => [],
              ]
          ]);
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->add("name")
            ->add("status")
            ->add("comment")
            ->add("price")
            ->add("complete")
            ->add("capacity");
    }
}