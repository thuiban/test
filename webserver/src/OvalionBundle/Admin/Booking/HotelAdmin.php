<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 04/11/2018
 * Time: 16:13
 */

namespace OvalionBundle\Admin\Booking;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class HotelAdmin extends AbstractAdmin
{
    protected function configureShowFields(ShowMapper $show)
    {
        $show->add("name")
            ->add("address")
            ->add("price");
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->add("name")
            ->add("price")
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->add("name")
            ->add("address")
            ->add("price");
    }
}