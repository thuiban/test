<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 04/11/2018
 * Time: 15:26
 */

namespace OvalionBundle\Admin\Account;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class OrderAdmin extends AbstractAdmin
{
 protected function configureListFields(ListMapper $list)
 {
     $list->add('user')
         ->add('travel')
         ->add('price')
         ->add('paymentref');
 }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
      $filter->add("user");
    }
}