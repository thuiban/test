<?php

use Doctrine\ORM\EntityManager;

/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 30/10/2018
 * Time: 12:55
 */

namespace OvalionBundle\Helper;


use OvalionBundle\Entity\Booking\Bus;
use OvalionBundle\Entity\Booking\Hotel;
use OvalionBundle\Entity\MyTeam\Match;
use OvalionBundle\Entity\Trips\Travel;
use Symfony\Component\VarDumper\VarDumper;

class TravelByOrder
{
    const ERROR_MATCH_NOT_SET = "Id match is not set.";
    const ERROR_DATE_NOT_SET = "Match date is not set.";

    /** @var EntityManager $entityManager */
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $data
     * @return Travel|string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create($data)
    {
        $hotelRepository = $this->entityManager->getRepository(Hotel::class);
        $busRepository = $this->entityManager->getRepository(Bus::class);
        $matchRepository = $this->entityManager->getRepository(Match::class);
        $travelRepository = $this->entityManager->getRepository(Travel::class);
        if (is_null($data[0]["idMatch"])) {
            return self::ERROR_MATCH_NOT_SET;
        }
        if (is_null($data[0]['date_start']) || is_null($data[0]['date_end'])) {
            return self::ERROR_DATE_NOT_SET;
        }
        /** @var Travel $newTravel */
        $newTravel = new Travel();
        VarDumper::dump($data[0]['idBus']);
        VarDumper::dump($busRepository->findOneBy(['id' => $data[0]['idBus']]));
        $newTravel->setBus($busRepository->findOneBy(['id' => $data[0]['idBus']]));
        $newTravel->setHotel($hotelRepository->findOneBy(['id' => $data[0]['idHotel']]));
        $newTravel->setMatch($matchRepository->findOneBy(['id' => $data[0]['idMatch']]));
        $newTravel->setType($data[0]['ticketType']);
        $newTravel->setDateEnd(new \DateTime($data[0]['date_end']));
        $newTravel->setDateStart(new \DateTime($data[0]['date_start']));
        $newTravel->setId(count($travelRepository->findAll()) + 1);
        $this->entityManager->persist($newTravel);
        $this->entityManager->flush();
        return $newTravel;
    }
}