<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 22/10/2018
 * Time: 15:34
 */

namespace OvalionBundle\Helper;

use Doctrine\ORM\EntityManager;
use OvalionBundle\Entity\Account\User;
use OvalionBundle\Entity\Parameters\Settings;

class UserByGoogle
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Function make to get User from Payload Google
     *
     * @param array $payload
     * @return User | object user
     * @throws \Doctrine\ORM\ORMException
     */
    public function getIt($payload)
    {
        $userRepository = $this->em->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneBy(["email" => $payload["email"]]);
        if (!$user) {
            $user = new User();
            $user->setEmail($payload["email"]);
            $user->setUsername($payload["name"]);
            $user->setRoles(['ROLE_USER']);
            $user->setEnabled(true);
            $user->setPlainPassword($payload['iat']);
            $user->setSettings(new Settings());
            $this->em->persist($user);
            $this->em->flush();
        }
        return $userRepository->findOneBy(["email" => $payload["email"]]);
    }

}