<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 25/10/2018
 * Time: 13:06
 */

namespace Tests\OvalionBundle\Entity\Booking;


use OvalionBundle\Entity\Booking\Bus;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class BusObjectTest extends TestCase
{
    const BASE_NAME = "SNCF";
    const BASE_PRICE = 42;
    const BASE_CAPACITY = 50;
    const BASE_COMPLETE = false;

    private function getBaseBus()
    {
        $bus = new Bus();
        $bus->setName(self::BASE_NAME);
        $bus->setPrice(self::BASE_PRICE);
        $bus->setCapacity(self::BASE_CAPACITY);
        $bus->setComplete(self::BASE_COMPLETE);
        return $bus;
    }

    public function testObjectIsNotNull()
    {
        $this->assertNotNull($this->getBaseBus());
    }

    public function testObjectDefaultSet()
    {
        $bus = $this->getBaseBus();
        $this->assertEquals($bus->getComment(), Bus::NO_COMMENT);
        $this->assertEquals($bus->getStatus(), Bus::BUS_ONTIME);
    }

    public function testNameSet()
    {
        $bus = $this->getBaseBus();
        $this->assertEquals($bus->getName(), self::BASE_NAME);
        $this->assertNotEquals($bus->getName(), "AirFrance");

        $bus->setName("EuroStar");
        $this->assertEquals("EuroStar", $bus->getName());
        $this->assertNotEquals(self::BASE_NAME, $bus->getName());
    }

    public function testPriceSet()
    {
        $bus = $this->getBaseBus();
        $this->assertEquals($bus->getPrice(), self::BASE_PRICE);
        $this->assertNotEquals($bus->getPrice(), 50);

        $bus->setPrice(50);
        $this->assertEquals(50, $bus->getPrice());
        $this->assertNotEquals(self::BASE_PRICE, $bus->getPrice());
    }

    public function testCapacitySet()
    {
        $bus = $this->getBaseBus();
        $this->assertEquals($bus->getCapacity(), self::BASE_CAPACITY);
        $this->assertNotEquals($bus->getCapacity(), 10000);

        $bus->setCapacity(1000);
        $this->assertEquals(1000, $bus->getCapacity());
        $this->assertNotEquals(self::BASE_CAPACITY, $bus->getCapacity());
    }

    public function testCompleteSet()
    {
        $bus = $this->getBaseBus();
        $this->assertEquals($bus->getComplete(), self::BASE_COMPLETE);
        $this->assertNotEquals($bus->getComplete(), true);

        $bus->setComplete(true);
        $this->assertEquals(true, $bus->getComplete());
        $this->assertNotEquals(self::BASE_COMPLETE, $bus->getComplete());
    }
}