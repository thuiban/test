<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 25/10/2018
 * Time: 13:43
 */

namespace Tests\OvalionBundle\Entity\Booking;


use OvalionBundle\Entity\Booking\Hotel;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class HotelObjectTest extends TestCase
{
    const BASE_NAME = "IBIS";
    const BASE_PRICE = 50;
    const BASE_PLACE = "far far away";

    private function getBaseHotel()
    {
        $hotel = new Hotel();
        $hotel->setName(self::BASE_NAME);
        $hotel->setPrice(self::BASE_PRICE);
        $hotel->setAddress(self::BASE_PLACE);

        return $hotel;
    }

    public function testObjectIsNotNull()
    {
        $this->assertNotNull($this->getBaseHotel());
    }

    public function testNameSet()
    {
        $hotel = $this->getBaseHotel();
        $this->assertEquals($hotel->getName(), self::BASE_NAME);
        $this->assertNotEquals($hotel->getName(), "Campanile");

        $hotel->setName("Kyriad");
        $this->assertEquals("Kyriad", $hotel->getName());
        $this->assertNotEquals(self::BASE_NAME, $hotel->getName());
    }

    public function testPriceSet()
    {
        $hotel = $this->getBaseHotel();
        $this->assertEquals($hotel->getPrice(), self::BASE_PRICE);
        $this->assertNotEquals($hotel->getPrice(), 40);

        $hotel->setPrice(40);
        $this->assertEquals(40, $hotel->getPrice());
        $this->assertNotEquals(self::BASE_PRICE, $hotel->getPrice());
    }

    public function testPlaceSet()
    {
        $hotel = $this->getBaseHotel();
        $this->assertEquals($hotel->getAddress(), self::BASE_PLACE);
        $this->assertNotEquals($hotel->getAddress(), "Not far far away");

        $hotel->setAddress("Bing");
        $this->assertEquals($hotel->getAddress(), "Bing");
        $this->assertNotEquals($hotel->getAddress(), self::BASE_PLACE);
    }
}