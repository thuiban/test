<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 24/10/2018
 * Time: 21:47
 */

namespace Tests\OvalionBundle\Entity\Booking;


use OvalionBundle\Entity\Booking\Bus;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BusControllerTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testSearchAll()
    {
        $buses = $this->entityManager->getRepository(Bus::class)->findAll();
        $this->assertNotCount(0, $buses);
    }

    public function testObjectIsSet()
    {
        $buses = $this->entityManager->getRepository(Bus::class)->findAll();
        /** @var Bus $bus */
        $bus = $buses[0];

        $this->assertNotNull($bus);
    }

    public function testObjectHaveId()
    {
        $buses = $this->entityManager->getRepository(Bus::class)->findAll();
        /** @var Bus $bus */
        foreach ($buses as $bus)
        {
            $this->assertNotNull($bus->getId());
        }
    }

    /**
     * @{inheritDoc}
     */
    protected function tearDown()
    {
       parent::tearDown();
       $this->entityManager->close();
       $this->entityManager = null;
    }
}