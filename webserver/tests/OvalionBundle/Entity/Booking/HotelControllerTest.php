<?php
/**
 * Created by PhpStorm.
 * User: thuiban
 * Date: 25/10/2018
 * Time: 13:36
 */

namespace Tests\OvalionBundle\Entity\Booking;


use OvalionBundle\Entity\Booking\Hotel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HotelControllerTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testSearchAll()
    {
        $hotels = $this->entityManager->getRepository(Hotel::class)->findAll();
        $this->assertNotCount(0, $hotels);
    }

    public function testObjectIsSet()
    {
        $hotels = $this->entityManager->getRepository(Hotel::class)->findAll();
        $this->assertNotNull($hotels[0]);
    }

    public function testObjectHaveId()
    {
        $hotels = $this->entityManager->getRepository(Hotel::class)->findAll();
        /** @var Hotel $hotel */
        foreach ($hotels as $hotel)
        {
            $this->assertNotNull($hotel->getId());
        }
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null;
    }
}